# Stupid Keyboard

This illustrates the process of training a simple machine learning model in the
browser to recognize letters "drawn" by dragging one's finger across keyboard keys.

Inspired by [this](https://www.youtube.com/watch?v=guJnFY1R4I0).