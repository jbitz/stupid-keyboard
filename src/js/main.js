document.addEventListener('DOMContentLoaded', async () => {
    const canvas = document.querySelector('#keyboard-grid');
    const ctx = canvas.getContext('2d');

    // Pressed symbols is a dictionary with symbols as the keys
    const drawKeys = (pressedSymbols) => {
        const SYMBOLS = [
            '1234567890-=',
            'qwertyuiop[]',
            'asdfghjkl;\'',
            'zxcvbnm,./'
        ];

        const HEIGHT = canvas.height;
        const WIDTH = canvas.width;
        const SPACING = 5;
        const KEY_SIZE = 50;
        const CORNER_RADIUS = 3;

        const X_OFFSET = 5;
        const Y_OFFSET = 5;

        // Clear past buffer
        ctx.clearRect(0, 0, WIDTH, HEIGHT);

        ctx.textAlign = "center"
        let row_start_x = X_OFFSET;
        for (let row = 0; row < SYMBOLS.length; row++) {
            for (let col = 0; col < SYMBOLS[row].length; col++) {
                const x_pos = X_OFFSET + row_start_x + (KEY_SIZE + SPACING) * col;
                const y_pos = Y_OFFSET + (KEY_SIZE + SPACING) * row;

                ctx.beginPath();
                ctx.roundRect(x_pos, y_pos, KEY_SIZE, KEY_SIZE, CORNER_RADIUS);

                const symbol = SYMBOLS[row][col];
                if (pressedSymbols[symbol] !== undefined) {
                    ctx.fill();
                } else {
                    ctx.stroke();
                }
                ctx.strokeText(
                    symbol, 
                    x_pos + KEY_SIZE / 2, 
                    y_pos + KEY_SIZE / 2
                );
            }
            row_start_x += KEY_SIZE / 2;
        }
    }
    drawKeys({}); // Initial canvas

    
    const RECOGNIZED_SYMBOLS = new Set('1234567890-=qwertyuiop[]asdfghjkl;\'zxcvbnm,./');
    
    // Let the user draw out a letter, then resolve the promise when they hit Enter
    // Returns the keyPresses object
    const recordLetter = () => {
        return new Promise((resolve, reject) => {
            const keyPresses = {}; // symbol => [event time(s)]
            let startTime = null;
            const keyListener = (e) => {
                if (startTime === null) {
                    startTime = Date.now();
                }
                const curTime = Date.now() - startTime;
                const key = e.key;

                if (key === 'Enter') {
                    resolve(keyPresses);
                    document.removeEventListener('keydown', keyListener);
                }
        
                if (RECOGNIZED_SYMBOLS.has(key)) {
                    if (keyPresses[key] !== undefined) {
                        keyPresses[key].push(curTime);
                    } else {
                        keyPresses[key] = [curTime];
                    }
                }
                drawKeys(keyPresses);
            };
            document.addEventListener('keydown', keyListener);
        });
    }
    
    const trainingData = {};
    const instructions = document.querySelector('#instructions');
    const REPETITIONS = 5;
    for (let chr = "a".charCodeAt(0); chr <= "b".charCodeAt(0); chr++) {
        letter = String.fromCharCode(chr);
        trainingData[letter] = [];
        for (let trialNum = REPETITIONS; trialNum > 0; trialNum--) {
            drawKeys({});
            instructions.innerHTML = `
            Draw the letter "${letter}" on your keyboard, then hit Enter:
            <br>
            (${trialNum} times remaining)
            `;
            trainingData[letter].push(await recordLetter());
        }
        
    }
    console.log(trainingData);
});